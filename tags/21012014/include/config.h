// Input buffer for (1/2)s at 44kHz
#define BUFFERSIZE 22000

/* The sample rate of the effects chain, modifiable at run time to ensure that the computation
*  completes
*/
uint32_t sampleRate;

/* Use looping buffer, the buffer starts at 
*  inputBuffer[inputBufferStart], loops from 
*  inputBuffer[BUFFERSIZE] to inputBuffer[0] then
*  carries on to inputBuffer[inputBufferStart - 1].
*  The most recent sample is stored at 
*  inputBuffer[inputBufferStart], and the oldest at
*  inputBuffer[inputBufferStart-1].
*/
uint8_t inputBuffer[BUFFERSIZE];
uint8_t inputBufferStart;

// Boolean value to describe whether the effects chain has successfully completed.
// Set to 0 at after input, set to 1 when output complete, set to 2 when effects computation complete.
uint8_t effectsComplete;

// For Debugging
char debugMessageBuffer[0xFF];
int debugMessageLength;