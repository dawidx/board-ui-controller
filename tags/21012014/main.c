/**
* Main file responsible for calling the controller
**/

#include "debug_frmwrk.h"		// Debug messages
#include "include/control.c"

void main(void)
{
	debug_frmwrk_init(); // Initialise the debug system to allow messages to be printed to the emulated terminal
	_DBG(	"\n\r\n\r>>>\n\r"
		"__________________________________________\n\r"
		"----------ADC->DAC Effects Filter---------\n\r"
		"__________________________________________\n\r"
		"\t\t(DAC output is Pin 18)\n\r\n\r"		
		"\t\t(ADC input is Pin 15)\n\r\n\r");
	_DBG("Entered the main/main()\n\r");
	start(); // Call to the control header
	
	while(1);
}