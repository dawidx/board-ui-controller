/**
* @file Distortion effect compresses the pick of the wave such that they are almost square
**/
#include <stdbool.h>
#include "distortion.h"

/**
* @desc The function returns the value of the sample when it passes through distortion effect
* @param uint8_t amplitude The sample value
**/
uint8_t Get_Distortion_Effect_Value(uint8_t amplitude, Filter filter)
{
	uint8_t value;
	//Threshld in both directions
	uint8_t max = 255 - Get_Distortion_Threshold(filter);
	uint8_t min = Get_Distortion_Threshold(filter);
	if(amplitude <= min)
	{
		value = min;
		//_DBG("Thresholding Applied\n\r");
	}
	else if (amplitude >= max) {
		value = max;
	}
	else// if(amplitude < g_DistortionThreshold)
	{
		value = amplitude;
	}
	return value;
}
/**
* @desc Generates a new distortion filter
* @return Filter
**/
Filter Generate_Distortion_Effect() {
	Filter filter;
	filter.ID = DISTORTION_EFFECT_ID;
	Set_Effect_Volume(100, filter);
	return filter;
}
/**
* @desc Sets the threshold for the distorion effect
* @return true if the value successfully applied, false otherwise (i.e. limit has been reached)
**/
bool Set_Distortion_Threshold(int8_t value, Filter filter) {
	if (value <= 128) {
		filter.Parameter[1] = value;
		return true;
	} else {
		return false;
	}
}
/**
* @desc Gets the threshold value
* @return uint8_t threshold value
**/
uint8_t Get_Distortion_Threshold(Filter filter) {
	return filter.Parameter[1];
}