/**
* @file This file contains methods related to the chorus effect
* @desc Chorus effect is like an echo effect except occurring very frequently. Just like
* voices in the choir can't sing all at exactly the same time. Some come earily some latter 
**/

#include "chorus.h"

/**
* @desc The method is based on the delay based on the g_chrusTime
**/
uint8_t Generate_Chorus_Value(uint8_t value, uint16_t samplingRate, Filter filter) {
	// _DBG("Generating the chorus value");
	//Saving the value to the buffer BEFORE applying any changes on the value to avoid echo effect
	filter.Buffer[filter.Index] = value;
	// _DBG("Stored the value in the buffer");
	//Calculating the offset
	uint16_t offset = Get_Chorus_Index_Offset(Generate_Chorus_Offset(filter, samplingRate), filter);
	// _DBG("Calculated offset");
	//Calculating the amplitude with the volumes of both voices taken in to account

	uint16_t amplitude = (Get_Chorus_Voice1_Volume(filter) * filter.Buffer[filter.Index]) / 100 + (Get_Chorus_Voice2_Volume(filter) * filter.Buffer[offset]) / 100;
	// _DBG("Calculated new amplitude");
	//Incrementing the index for the next
	filter.Index = Get_Chorus_Index_Offset(1, filter);
	// _DBG("Updated the index");
	//returning the output amplitude value
	return (uint8_t)(amplitude >> 8);
}
/**
* @desc The function generates new chorus effect
* @return Filter chorus
**/
Filter Generate_Chorus_Effect() {
	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Effect()\n\r");
	Filter filter;
	filter.ID = CHORUS_EFFECT_ID;
	filter.Index = 0;
	// _DBG("Set the ID of Chorus Effect\n\r");
	//Volume, ChorusTime, Volume of Voice 1, Volume of Voice 2;
	Set_Effect_Volume(100, filter);
	// _DBG("Set the Volume of the Chorus Effect\n\r");
	Set_Chorus_Time_Value(50, filter);
	// _DBG("Set Chorus Time\n\r");
	Set_Chorus_Voice1_Volume(100, filter);
	// _DBG("Set The volume of the second guitar\n\r");
	Set_Chorus_Voice2_Volume(100, filter);
	// _DBG("Set the volume of the second guitar\n\r");
	//Due to limited memory we can allocate the maximim "delay" of 50ms at 44 kHz
	static uint8_t effectArray[2200];
	filter.Buffer = effectArray;
	// _DBG("Allocated memory\n\r");


	return filter;
}
/**
* @desc The function generates the offset value based on the sampling rate and current chorus time value
* @return uint16_t Offset
**/
uint16_t Generate_Chorus_Offset(Filter filter, uint16_t samplingRate) {
	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Offset()\n\r");
	return Get_Chorus_Time_Value(filter) * samplingRate / 1000;
}

/**
* @desc Function used for calculating Index in the chorus buffer
* @return Index
**/
uint16_t Get_Chorus_Index_Offset(uint16_t offset, Filter filter) {
	//_DBG("Entered the effects/chorus.c/Get_Chorus_Index_Offset()\n\r");
	if ((filter.Index + offset) >= 2200) {
		return 2200 - (filter.Index + offset);
	}
	else {
		return filter.Index + offset;
	}
}

/**
* @desc The function sets the value of the chorus effect i.e. the microseconds the signal is delayed
* @return bool true if assignment successful, false otherwise
**/
bool Set_Chorus_Time_Value(uint8_t value, Filter filter) {

	// _DBG("Entered the effects/chorus.c/Set_Chorus_Time_Value()\n\r");
	if (value <= 50 && value >= 0) {
		filter.Parameter[1] = value;
		return true;
	}
	else
	{
		return false;
	}
}
/**
* @desc Functions used for extracting Chorus Time i.e. by how many microseconds the signal is delayed
* @return uint8_t the value of the chorus time
**/
uint8_t Get_Chorus_Time_Value(Filter filter) {
	return filter.Parameter[1];
}



/**
* @desc Sets the value for volume for the original signal
* @return bool true if assignment successful, false otherwise (i.e. maximum volume)
**/
bool Set_Chorus_Voice1_Volume(uint8_t value, Filter filter) {

	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Voice1_Volume()\n\r");
	if (value <= 100 && value >= 0) {
		filter.Parameter[2] = value;
		return true;
	}
	else {
		return false;
	}
}
/**
* @desc gets the value for volume of the original signal
**/
uint8_t Get_Chorus_Voice1_Volume(Filter filter) {
	return filter.Parameter[2];
}

/**
* @desc Sets the value for volume for the delayed signal
* @return bool true if assignment successful, false otherwise (i.e. maximum volume)
**/
bool Set_Chorus_Voice2_Volume(uint8_t value, Filter filter) {
	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Voice2_Volume()\n\r");
	if (value <= 100 && value >= 0) {
		filter.Parameter[3] = value;
		return true;
	}
	else {
		return false;
	}
}

/**
* @desc gets the value for volume of the delayed signal
* @return 
**/
uint8_t Get_Chorus_Voice2_Volume(Filter filter) {
	return filter.Parameter[3];
}