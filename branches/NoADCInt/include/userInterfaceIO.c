#include "userInterfaceIO.h"
#include "lpc17xx_uart.h"		// Central include files
#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "lpc17xx_i2c.h"
#include "debug_frmwrk.h"		// Debug messages
#include <stdio.h>

#define I2CDEV_S_ADDR_LCD	(0x3B)	// LCD Display Controller address
#define I2CDEV_S_ADDR_KEYPAD (0x21) // Keypad interface address
#define I2CDEV_M LPC_I2C1		// I2C Interface

char g_KeyPadInput;
bool g_KeyPadTransferComplete;
uint8_t g_KeypadRecieveBuffer[1];

__IO FlagStatus complete;
typedef enum {NO_TRANSFER, LCD_TRANSFER, KEYPAD_TRANSFER} i2c_transferStatus_t;
i2c_transferStatus_t g_i2c_transferStatus = NO_TRANSFER;

void setI2CTransferStatus(i2c_transferStatus_t status)
{
	g_i2c_transferStatus = status;

	if (status == NO_TRANSFER)
	{
		complete = SET;
	}
	else
	{
		complete = RESET;
	}
}

i2c_transferStatus_t getI2CTransferStatus()
{
	return g_i2c_transferStatus;
}

// Blocks until the i2c transfer status is NO_TRANSFER, then set it to the status passed in the parameter.
void blockAndSetI2CTransferStatus(i2c_transferStatus_t status)
{
	while (getI2CTransferStatus() != NO_TRANSFER);

	g_i2c_transferStatus = status;
}

void i2c_init(void)
{
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;

	if (I2CDEV_M  == LPC_I2C0)
	{
		// Interrupt setup
		// Disable I2C0 interrupt
		NVIC_DisableIRQ(I2C0_IRQn);
		// preemption = 1, sub-priority = 1
		NVIC_SetPriority(I2C0_IRQn, ((0x01<<3)|0x01));

		// I2C Device Pins
		// Doesn't work
		PinCfg.Funcnum = 3;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 1;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 0;
		PINSEL_ConfigPin(&PinCfg);
	}

	if (I2CDEV_M  == LPC_I2C1) // Works
	{
		// Interrupt setup
		// Disable I2C0 interrupt
		NVIC_DisableIRQ(I2C1_IRQn);
		// preemption = 1, sub-priority = 1
		NVIC_SetPriority(I2C1_IRQn, ((0x01<<3)|0x01));

		// I2C Device Pins
		PinCfg.Funcnum = 3;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 1;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 0;
		PINSEL_ConfigPin(&PinCfg);
	}

	if (I2CDEV_M  == LPC_I2C2) // Works
	{
		// Interrupt setup
		// Disable I2C0 interrupt
		NVIC_DisableIRQ(I2C2_IRQn);
		// preemption = 1, sub-priority = 1
		NVIC_SetPriority(I2C2_IRQn, ((0x01<<3)|0x01));

		// I2C Device Pins
		PinCfg.Funcnum = 2;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 10;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 11;
		PINSEL_ConfigPin(&PinCfg);
	}
	// Initialize Master I2C peripheral
    I2C_Init(I2CDEV_M, 100000);

	// Enable Master I2C operation
    I2C_Cmd(I2CDEV_M, ENABLE);

	if (I2CDEV_M  == LPC_I2C0)
	{
    	NVIC_EnableIRQ(I2C0_IRQn);
		_DBG("Interrupt for I2C enabled 0 \n\r");
    }
    if (I2CDEV_M  == LPC_I2C1)
	{
    	NVIC_EnableIRQ(I2C1_IRQn);
		_DBG("Interrupt for I2C enabled 1 \n\r");
    }
    if (I2CDEV_M  == LPC_I2C2)
	{
    	NVIC_EnableIRQ(I2C2_IRQn);
		_DBG("Interrupt for I2C enabled 2 \n\r");
    }
	//I2C_DeInit(I2CDEV_M);
}

void LCD_init(void)
{
	// Buffer for transmission data
	uint8_t Transmit_Buffer[0x12];

	// Fill buffer with data
	
	Transmit_Buffer[0] = 0x04;	// Control: Function Set
	Transmit_Buffer[1] = 0x34;	// Function Set
	Transmit_Buffer[2] = 0x0D;	// Display & Cursor On
	Transmit_Buffer[3] = 0x06;	// Entry Mode Set
	Transmit_Buffer[4] = 0x35;
	Transmit_Buffer[5] = 0x04;
	Transmit_Buffer[6] = 0x10;
	Transmit_Buffer[7] = 0x42; // HV Stages 3
	Transmit_Buffer[8] = 0x9f; // set Vlcd, store to VA
	Transmit_Buffer[9] = 0x34; // DL: 8 bits, M:  two line, SL: 1:18, H: normal instruction set
	Transmit_Buffer[10] = 0x80; // DDRAM Address set to 0x00
	Transmit_Buffer[11] = 0x02; // return home

	// Send data
	if(DEBUG)
		_DBG("Begin transmit LCD initialisation data\n\r");
	sendDataLCD(Transmit_Buffer, 12);
	if(DEBUG)
		_DBG("End transmit LCD initialisation data\n\r");
}

// Returns 1 if successful
int sendDataLCD(uint8_t Transmit_Buffer[], int Length)
{	
	

	I2C_M_SETUP_Type transferMCfg;	

	// Start slave device
	transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_LCD;
	transferMCfg.tx_data = Transmit_Buffer;
	transferMCfg.tx_length = Length;
	transferMCfg.rx_data = NULL;
	transferMCfg.rx_length = 0;

	transferMCfg.retransmissions_max = 3;

	if(DEBUG)
		_DBG("Begin blocking and changing I2C status\n\r");

	blockAndSetI2CTransferStatus(LCD_TRANSFER);

	if(DEBUG)
		_DBG("Transfer data now\n\r");
	I2C_MasterTransferData(I2CDEV_M, &transferMCfg, I2C_TRANSFER_INTERRUPT);
	if(DEBUG)
		_DBG("Transferred data now\n\r");

	return 0;
}

/**
* @desc Writes the char transmit to the end of the LCD, moves on to the
* next position. If at end of line, move to next line; loop.
* Efficient method.
**/
int LCD_transmit_char(char transmit)
{
	char debugMessageBuffer[0xFF];
	int debugMessageLength;	

	uint8_t Transmit_Buffer[1];
	uint8_t Recieve_Buffer[1];

	// Read current address

	// Setup device
	I2C_M_SETUP_Type transferMCfg;

	Transmit_Buffer[0] = 0x00;	// Control Byte: Function Set: Read Current Address

	// Start slave device
	transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_LCD;
	transferMCfg.tx_data = Transmit_Buffer;
	transferMCfg.tx_length = 1;
	transferMCfg.rx_data = Recieve_Buffer;
	transferMCfg.rx_length = 1;

	transferMCfg.retransmissions_max = 3;

	blockAndSetI2CTransferStatus(LCD_TRANSFER);

	Status status = I2C_MasterTransferData(I2CDEV_M, &transferMCfg, I2C_TRANSFER_INTERRUPT);

	if (status == SUCCESS)
	{
		/*debugMessageLength = sprintf(debugMessageBuffer, "Success: Status: 0x%x\n\rRecieved Data: 0x%x\n\r", status, Recieve_Buffer[0]);
		_DBG(debugMessageBuffer);*/
		uint8_t cursorAddress = Recieve_Buffer[0]; // Got current cursor address
		uint8_t Transmit_Buffer0[2];
		if ((cursorAddress > 0x0F) && (cursorAddress < 0x28))
		{
			// Go to second line
			Transmit_Buffer0[0] = 0x00;	// Control: Function Set
			Transmit_Buffer0[1] = 0xC0;	// Go to position 40
			sendDataLCD(Transmit_Buffer0, 2);
		}
		else if(cursorAddress > 0x4F)
		{
			// Set address to 0 / go home
			/*debugMessageLength = sprintf(debugMessageBuffer, "Returning to first line\n\r");
			_DBG(debugMessageBuffer);*/
			Transmit_Buffer0[0] = 0x00;	// Control: Function Set
			Transmit_Buffer0[1] = 0x80;	// Go to position 0
			sendDataLCD(Transmit_Buffer0, 2);
		}

		Transmit_Buffer0[0] = 0x44; // Control: Write Mode
		Transmit_Buffer0[1] = (transmit + 0x80); // Data byte to transmit = ASCII code + 0x80
		sendDataLCD(Transmit_Buffer0, 2);

		return 0;
	}
	else if (status == ERROR)
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Transmission Error:\nStatus: 0x%x\n\r\n\r\n\r", status);
		_DBG(debugMessageBuffer);
		return -1;
	}
	else
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Status: 0x%x\n\r\n\r", status);
		_DBG(debugMessageBuffer);
		return -1;
	}
}

/**
* @desc Writes a string up to 16 characters to the LCD, starting at the
* current position. Efficient method.
**/
int LCD_transmit_string(char* string, uint8_t length)
{
	int i;
	if (string[length-1] == '\0')
	{

		// debugMessageLength = sprintf(debugMessageBuffer, "Length = %d string[length-2] = %c\n\r", length, string[length-2]);
		// _DBG(debugMessageBuffer);

		uint8_t Transmit_Buffer[length + 1];
		Transmit_Buffer[0] = 0x44;	// Control Byte: Function Set: Write Data

		for (i = 0; i < length-1; ++i)
		{
			/*debugMessageLength = sprintf(debugMessageBuffer, "Char Number = 0x%x, Char = %c\n\r", i, string[i]);
			_DBG(debugMessageBuffer);*/
			Transmit_Buffer[i+1] = (string[i] + 0x80); // Data byte to transmit = ASCII code + 0x80
		}
		// debugMessageLength = sprintf(debugMessageBuffer, "\n\rSuccess = %d\n\r", sendDataLCD(Transmit_Buffer, length));
		// _DBG(debugMessageBuffer);
		sendDataLCD(Transmit_Buffer, length);
		return 0; // Success
	}
	else
	{
		return - 1; // Incorrect string length
	}
}

/**
* @desc Maintains a buffer of the LCD screen contents, and updates a single
* character in a desired position.
* Inefficient method as re-sends the entire
* screen's data.
* Also, will not work if other screen update methods are called
* as only this function updates the buffer.
* 
* Perhaps should remove / re-write?
**/
int updateLCD(char toUpdate, int position)
{
	static char line1[]="                ";
	static char line2[]="                ";

	_DBG("Updating LCD\n\r");
	char debugMessageBuffer[0xFF];
	int debugMessageLength;
	debugMessageLength = sprintf(debugMessageBuffer, "%d\n\r",position);
	_DBG(debugMessageBuffer);
	if(position<16)
	{
		line1[position]=toUpdate;
	}
	else
	{
		line2[position-16]=toUpdate;
	}
	position++;
	position = position%32;

	_DBG("Updating LCD\n\r");		
	LCD_transmit_string(line1, 17);
	LCD_transmit_newline();
	LCD_transmit_string(line2, 17);
	LCD_transmit_newline();

	return position;
}

/**
* @desc Moves the current position to the start of the line not currently selected.
**/
int LCD_transmit_newline()
{
	char debugMessageBuffer[0xFF];
	int debugMessageLength;	

	uint8_t Transmit_Buffer[1];
	uint8_t Recieve_Buffer[1];

	// Read current address

	// Setup device
	I2C_M_SETUP_Type transferMCfg;

	Transmit_Buffer[0] = 0x00;	// Control Byte: Function Set: Read Current Address

	// Start slave device
	transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_LCD;
	transferMCfg.tx_data = Transmit_Buffer;
	transferMCfg.tx_length = 1;
	transferMCfg.rx_data = Recieve_Buffer;
	transferMCfg.rx_length = 1;

	transferMCfg.retransmissions_max = 3;

	blockAndSetI2CTransferStatus(LCD_TRANSFER);

	Status status = I2C_MasterTransferData(I2CDEV_M, &transferMCfg, I2C_TRANSFER_INTERRUPT);

	if (status == SUCCESS)
	{
		/*debugMessageLength = sprintf(debugMessageBuffer, "Success: Status: 0x%x\n\rRecieved Data: 0x%x\n\r", status, Recieve_Buffer[0]);
		_DBG(debugMessageBuffer);*/
		uint8_t cursorAddress = Recieve_Buffer[0]; // Got current cursor address
		uint8_t Transmit_Buffer0[2];
		if (cursorAddress < 0x40)
		{
			// Set address to 40;
			Transmit_Buffer0[0] = 0x00;	// Control: Function Set
			Transmit_Buffer0[1] = 0xC0;	// Go to position 40
			sendDataLCD(Transmit_Buffer0, 2);

		}
		else
		{
			// Set address to 0 / go home
			Transmit_Buffer0[0] = 0x00;	// Control: Function Set
			Transmit_Buffer0[1] = 0x80;	// Go to position 0
			sendDataLCD(Transmit_Buffer0, 2);
		}

	}
	else if (status == ERROR)
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Transmission Error:\nStatus: 0x%x\n\r\n\r\n\r", status);
		_DBG(debugMessageBuffer);
		return -1;
	}
	else
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Status: 0x%x\n\r\n\r", status);
		_DBG(debugMessageBuffer);
		return -1;
	}


	return 0; // Success
}


// void Print_Line(char *string, int line_num)
// {
// 	LCD_transmit_string(&string, 17);
// }


/**
* @desc Method 0 calls the screen clear LCD controller function, replacing the screen
* with *s. Method 1 writes two lines of ' ' to the LCD, effectively clearing the screen,
* before returning the current position to the start of the screen.
**/
bool LCD_screen_clear(int method)
{
	// Method 0 uses clear display command, which fills the screen with *s, method 1 fills the screen with " " charactors.
	
	uint8_t Transmit_Buffer[2];

	if (method == 0)
	{
		Transmit_Buffer[0] = 0x04;	// Control Byte: Function Set: Write Data

		Transmit_Buffer[1] = (0x01); // Clear display command
		
		sendDataLCD(Transmit_Buffer, 2);
		return true; // Success
	}
	else if (method == 1)
	{
		if(DEBUG)
		{
			_DBG("Running LCD_screen_clear using method 1\n\r");
		}
		// Call method 0 to return to start
		Transmit_Buffer[0] = 0x00;	// Control: Function Set
		Transmit_Buffer[1] = 0x80;	// Go to position 0
		
		sendDataLCD(Transmit_Buffer, 2);

		LCD_transmit_string("                ", 17);
		LCD_transmit_newline();
		LCD_transmit_string("                ", 17);
		LCD_transmit_newline();
		return true; // Success
	}
	else
	{
		return false; // Incorrect parameter passed
	}
}

/**
* @desc Takes a column to check for key presses, and returns the pressed keys for that column
**/
// Returns values for each row, takes values to check each column
bool checkKeypad(uint8_t columnValues)
{
	uint8_t Transmit_Buffer[1];

	Transmit_Buffer[0] = columnValues;

	I2C_M_SETUP_Type transferMCfg;

	// Start slave device
	transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_KEYPAD;
	transferMCfg.tx_data = Transmit_Buffer;
	transferMCfg.tx_length = 1;
	transferMCfg.rx_data = g_KeypadRecieveBuffer;
	transferMCfg.rx_length = 1;
	// debugMessageLength = sprintf(debugMessageBuffer, "Data Sent: 0x%x\t", *Transmit_Buffer);
	// _DBG(debugMessageBuffer);
	//_DBG("Buffer:\n\r");

	// debugMessageLength = sprintf(debugMessageBuffer, "\t0x%x\n\r", Transmit_Buffer[0]);
	// _DBG(debugMessageBuffer);
	
	transferMCfg.retransmissions_max = 3;
	g_KeyPadTransferComplete = false;

	blockAndSetI2CTransferStatus(KEYPAD_TRANSFER);

	Status status = I2C_MasterTransferData(I2CDEV_M, &transferMCfg, I2C_TRANSFER_INTERRUPT);

	if (status == SUCCESS)
	{
	// 	debugMessageLength = sprintf(debugMessageBuffer, "Success: Status: 0x%x\n\r", status);
	// 	_DBG(debugMessageBuffer);
		/*debugMessageLength = sprintf(debugMessageBuffer, "Data Recieved: 0x%x\n\r", *Recieve_Buffer);
		_DBG(debugMessageBuffer);*/
		//return(((*Recieve_Buffer) & 0x0F));
		return true;
		//return *Recieve_Buffer;
	}
	else if (status == ERROR)
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Transmission Error:\nStatus: 0x%x\n\r\n\r\n\r", status);
		_DBG(debugMessageBuffer);
		return false;
	}
	else
	{
		debugMessageLength = sprintf(debugMessageBuffer, "I2C Transmission Error Status: 0x%x\n\r\n\r", status);
		_DBG(debugMessageBuffer);
		return false;
	}
}

void I2C1_IRQHandler(void)
{
	_DBG("I2C Interrupt Handler Called\n\r");

	if (getI2CTransferStatus() == KEYPAD_TRANSFER)
	{
		g_KeyPadTransferComplete = true;

		g_KeyPadInput = keypadCharFromCode(g_KeypadRecieveBuffer[0]);

		static char previousPress[]={' ',' ',' ',' '}; // Previous press for each column
		
		uint8_t row;
		switch(g_KeyPadInput && 0xF0)
		{
			case(0x70):
				row = 0;
				break;
			case(0xb0):
				row = 1;
				break;

			case(0xd0):
				row = 2;
				break;
			case(0xe0):
				row = 3;
				break;
		}

		if ((previousPress[row] != ' ') || (previousPress[row] == g_KeyPadInput))
		{
			g_KeyPadInput = ' ';
		}
	}

	setI2CTransferStatus(NO_TRANSFER);
}

void I2C0_IRQHandler(void)
{
	if(DEBUG)
		_DBG("Int on I2C device 0\n\r");
	I2C1_IRQHandler();
}

void I2C2_IRQHandler(void)
{
	if(DEBUG)
		_DBG("Int on I2C device 2\n\r");
	I2C1_IRQHandler();
}

/**
* @desc Takes 8 bit key code (rows selected and columns selected) and returns equivalent value
* char to the key label.
**/
char keypadCharFromCode(char keyReceived)
{
	char out;
	switch(keyReceived)
	{
		case(0x77):
			out='1';
			break;
		case(0xb7):
			out='2';
			break;
		case(0xd7):
			out='3';
			break;
		case(0xe7):
			out='A';
			break;
		case(0x7b):
			out='4';
			break;
		case(0xbb):
			out='5';
			break;
		case(0xdb):
			out='6';
			break;
		case(0xeb):
			out='B';
			break;
		case(0x7d):
			out='7';
			break;
		case(0xbd):
			out='8';
			break;
		case(0xdd):
			out='9';
			break;
		case(0xed):
			out='C';
			break;
		case(0x7e):
			out='*';
			break;
		case(0xbe):
			out='0';
			break;
		case(0xde):
			out='#';
			break;
		case(0xee):
			out='D';
			break;
		default:
			out=' ';
			break;

	}

	return out;
}

/**
* @desc This method checks the keyboard, and returns the char pressed, provided only
* one button at a time is pressed. It returns 'X' if more than one button is pressed,
* and ' ' if no buttons are pressed.
**/
char getInput()
{
	static uint8_t columns[4]={0x7F,0xBF,0xDF,0xEF};

	if (g_KeyPadTransferComplete == true)
	{
		static int row = 0;
		checkKeypad(columns[row]);
		if (row <= 3)
		{
			row++;
		}
		else
		{
			row = 0;
		}

		return g_KeyPadInput;
	}
	else
	{
		return ' ';
	}
}