uint32_t reduceSampleRate(uint32_t samplingRate);
void applyEffects();
void Error_Report(char* message, uint8_t severity);
uint16_t Get_Location_Offset(uint16_t offset);