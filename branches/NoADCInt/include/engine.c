#include "engine.h"
//#include "../effects/filter.h"
#include "../effects/distortion.c"
#include "../effects/passive.c"
#include "../effects/echo.c"
#include "../effects/chorus.c"



uint8_t g_MasterVolume = 100;

uint8_t g_AvailableEffects[] = { 0, 1, 2, 3 };

/**
* @desc the array with all the filters. Intitially, Reset_Filters() should be applied to set all the filters to passive ones
**/
Filter filters[MAX_PARALLEL][MAX_SERIES];

/**
* @desc Method applies appropiate effect depending on the given filter
* @param struct *filter is the filter
* @param uint8_t value is the value to be modified
**/
uint8_t Apply_Effect(Filter filter, uint8_t value)
{
	switch (filter.ID)
	{
		//Passive filter
	case PASSIVE_EFFECT_ID:
		return passiveEffectValue(value);
	case DISTORTION_EFFECT_ID:
		return Get_Distortion_Effect_Value(value, filter);
	case CHORUS_EFFECT_ID:
		return Generate_Chorus_Value(value, g_SampleRate, filter);
	case ECHO_EFFECT_ID:
		return Get_Echo_Value(value, g_SampleRate, filter);
	default:
		return value;
	}
}


/**
* @desc Called for each sample, applys the effect table (which may combine multiple effects), preparing it for output.
**/
uint8_t Apply_Effects(uint8_t value)
{
	//Defining the array for each channel. The values stored represent value for each channel. Each channel gets the same input
	uint16_t channels[MAX_PARALLEL] = { value, value, value, value };
	// uint16_t volumes[MAX_PARALLEL] = { 0, 0, 0, 0 };
	//Enumerating through each channel in series
	uint16_t series, parallel, new_value;
	//foreach channel
	for (parallel = 0; parallel < MAX_PARALLEL; parallel++)
	{
		//Add elements in series
		for (series = 0; series < MAX_SERIES; series++)
		{
			//the elements are in series so we feed the previous value with relevant filter and multiply it by the value
			channels[parallel] = Apply_Effect(filters[parallel][series], channels[parallel]) * (Get_Effect_Volume(filters[parallel][series]) / 100);

		}
		//adding the channel
		new_value += (channels[parallel] >> 2); // Average of 4
	}
	//Taking an average 
	new_value = new_value * (Get_Volume() / 100);
	return (uint8_t) new_value;
}

/* In turn, requests an updated table for each effect that has had
*  parameters changed, or been activated or deactivated. It then combines
*  these tables, returning the combined effect table that is applied to
*  each sample.
*/
void applyChanges()
{

	// Request updated effects table from each effect

	// Combine effects tables

	// Return combined effects table

}

void Reset_Filters()
{
	//Setting up every fiter to passthrough
	int a, b;
	for (a = 0; a < MAX_PARALLEL; a++) {
		for (b = 0; b < MAX_SERIES; b++) {
			filters[a][b].ID = 0;
			filters[a][b].Parameter[0] = 100;
		}
	}
}

bool Set_Effect_Volume(uint8_t volume, Filter filter) {
	if (volume <= 100) {
		filter.Parameter[0] = volume;
		return true;
	}
	else {
		return false;
	}
}

uint8_t Get_Effect_Volume(Filter filter) {
	return filter.Parameter[0];
}

uint8_t Get_Volume() {
	return g_MasterVolume;
}

bool IncreaseVolume(uint8_t currentVolume) {
	if (currentVolume <= 95) {
		SetVolume(currentVolume + 5);
		return true;
	}
	else {
		return false;
	}
}

bool DecreaseVolume(uint8_t currentVolume) {
	if (currentVolume >= 5) {
		SetVolume(currentVolume - 5);
		return true;
	}
	else {
		return false;
	}
}

bool SetVolume(uint8_t volume) {
	if (volume <= 100 && volume >= 0) {
		g_MasterVolume = volume;
		return true;
	}
	else {
		return false;
	}
}

char* Get_EffectName(uint8_t effectID) {
	switch (effectID) {
	case PASSIVE_EFFECT_ID:
		return "<Passive       >";
	case DISTORTION_EFFECT_ID:
		return "<Distortion    >";
	case CHORUS_EFFECT_ID:
		return "<Chorus        >";
	case ECHO_EFFECT_ID:
		return "<Echo          >";
	default:
		return "Error           ";
	}
}

uint8_t Get_EffectID(uint8_t channel, uint8_t location) {
	return filters[channel][location].ID;
}

uint8_t* Get_AllAvailableEffects(uint8_t channel, uint8_t location) {
	return g_AvailableEffects;
}

bool ResetEffectDefaults(uint8_t channel, uint8_t location) {
	uint8_t id = filters[channel][location].ID;
	//Freeing up the memory spac
	free(filters[channel][location].Buffer);
	SetEffect(channel, location, id);
	return true;
}

bool ClearEffect(uint8_t channel, uint8_t location) {
	//freeing up memory
	free(filters[channel][location].Buffer);
	//set it to passive
	SetEffect(channel, location, 0);
	return true;
}


bool SetEffect(uint8_t channel, uint8_t location, uint8_t effectID) {
	char debugMessageBuffer[0xFF];
	int debugMessageLength;

	debugMessageLength = sprintf(debugMessageBuffer, "SetEffect:189\n\r\tCalled SetEffect with variables:"
		"\n\r\t\tchannel: %d\n\r\t\tlocation: %d\n\r\t\teffectID: %d\n\r",channel, location, effectID);
	_DBG(debugMessageBuffer);
	switch (effectID)
	{
		case PASSIVE_EFFECT_ID:
			filters[channel][location] = Generate_Passive_Effect();
			return true;
		case DISTORTION_EFFECT_ID:
			filters[channel][location] = Generate_Distortion_Effect();
			return true; 
		case CHORUS_EFFECT_ID:
			filters[channel][location] = Generate_Chorus_Effect();
			// _DBG("SetEffect:202\n\r\tReturned\n\r");
			return true;
		case ECHO_EFFECT_ID:
			filters[channel][location] = Generate_Echo_Effect();
			return true;
		default:
			filters[channel][location] = Generate_Passive_Effect();
			return false;
	}
}

uint8_t Get_NumberEffectVariables(uint8_t effectID) {
	switch (effectID){
	case PASSIVE_EFFECT_ID:
		return 1;
	case DISTORTION_EFFECT_ID:
		return 2;
	case CHORUS_EFFECT_ID:
		return 4;
	case ECHO_EFFECT_ID:
		return 3;
	default:
		return 0;
	}
}

char* Get_EffectVariableName(uint8_t effectID, uint8_t variable) {
	if (variable == 0) {
		return "Volume";
	}
	else {
		switch (effectID){
		case DISTORTION_EFFECT_ID:
			return "Threshold       ";
		case CHORUS_EFFECT_ID:
			if (variable == 1) {
				return "Delay           ";
			}
			else if (variable == 2) {
				return "Volume Guitar 1";
			}
			else if (variable == 3) {
				return "Volume Guitar 2";
			}
			else {
				return "Error           ";
			}
			break;
		case ECHO_EFFECT_ID:
			if (variable == 1) {
				return "Strength        ";
			}
			else if (variable == 2) {
				return "Delay           ";
			} else {
				return "Error           ";
			}
			break;
		default:
			return "Error           ";
			break;
		}
	}
}

uint8_t Get_EffectVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber)
{
	return filters[channel][location].Parameter[variableNumber];
}


bool IncrementVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber)
{
	Filter filter = filters[channel][location];

	if (variableNumber == 0)
	{
		Set_Effect_Volume(Get_Effect_Volume(filter) + 5, filter);
		return true;
	}
	else {
		switch (filter.ID){
		case DISTORTION_EFFECT_ID:
			Set_Chorus_Time_Value(Get_Chorus_Time_Value(filter) + 5, filter);
			return true;
		case CHORUS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Chorus_Time_Value(Get_Chorus_Time_Value(filter) + 5, filter);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Chorus_Voice1_Volume(Get_Chorus_Voice1_Volume(filter) + 5, filter);
				return true;
			}
			else if (variableNumber == 3) {
				Set_Chorus_Voice2_Volume(Get_Chorus_Voice2_Volume(filter) + 5, filter);
				return true;
			}
			else {
				return false;
			}
		case ECHO_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Echo_Strength(Get_Echo_Strength(filter), filter);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Echo_Time(Get_Echo_Time(filter), filter);
				return true;
			}
			else {
				return false;
			}
		}
	}
	return true;
}

bool DecrementVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber) {
	Filter filter = filters[channel][location];

	if (variableNumber == 0) {
		Set_Effect_Volume(Get_Effect_Volume(filter) - 5, filter);
		return true;
	}
	else {
		switch (filter.ID){
		case DISTORTION_EFFECT_ID:
			Set_Chorus_Time_Value(Get_Distortion_Threshold(filter) - 5, filter);
			return true;
		case CHORUS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Chorus_Time_Value(Get_Chorus_Time_Value(filter) - 5, filter);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Chorus_Voice1_Volume(Get_Chorus_Voice1_Volume(filter) - 5, filter);
				return true;
			}
			else if (variableNumber == 3) {
				Set_Chorus_Voice2_Volume(Get_Chorus_Voice2_Volume(filter) - 5, filter);
				return true;
			}
			else {
				return false;
			}
		case ECHO_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Echo_Strength(Get_Echo_Strength(filter), filter);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Echo_Time(Get_Echo_Time(filter), filter);
				return true;
			}
			else {
				return false;
			}
		}
	}
	return true;
}

uint8_t Get_NumEffects(uint8_t channel, uint8_t location) {
	return sizeof(g_AvailableEffects) / sizeof(g_AvailableEffects[0]);
}
