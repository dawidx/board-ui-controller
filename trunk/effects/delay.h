/**
* @desc The function generates the new value for the filter
* @param uint8_t value the value from the previous effect
* @param uint16_t samplingRate the effect has internal memory and it needs to know which memory location to access
* @param Filter *filter the parameters of the effect
* @return uint8_t
**/
uint8_t Get_Delay_Value(uint8_t value, uint16_t samplingRate, Filter *filter);
/**
* @desc The function generates new filter with default values
* @return Pointer to the new filter
**/
Filter Generate_Delay_Effect();
/**
* @desc Sets the value for the strength. Maximum 100 (no input signal), Min 0 (no Delay)
* @param uint8_t value the new value for the strength
* @param Filter *filter the pointer to the effect
* @return bool True if the assignment successful
**/
bool Set_Delay_Strength(uint8_t value, Filter *filter);
/**
* @desc Gets the current value of strength
* @param Filter *filter, pointer to the filter
* @return uint8_t value, value between 0 and 100
**/
uint8_t Get_Delay_Strength(Filter *filter);
/**
* @desc Sets the time of the delay
* @param uint8_t value. Maximum time of delay is 500
* @param Filter *filter. The pointer to the effect
* @return bool True if assignment successful
**/
bool Set_Delay_Time(uint8_t value, Filter *filter);
/**
* @desc Gets the current value of the delay in miliseconds
* @param Filter *filter. The pointer to the effect
* @return uint8_t current value in miliseconds
**/
uint8_t Get_Delay_Time(Filter *filter);
/**
* @desc The function calculates and returns the memory location with given offset. 
* @param uint16_t offset the number of locations before the current sample
**/
uint16_t Get_Delay_Buffer_Offset(uint16_t offset, uint16_t max, Filter *filter);