/**
* @desc struct filter stores the relevant information about filter
* int ID the ID of the effect
* int Parameter[] holds all the required parameters. These should only be changed via special methods never directly
* int Buffer[] acts as internal memory for the effect
* int Index pointing to the current location 
**/
typedef struct filter {
	uint8_t ID;
	//First parameter is ALWAYS the volume
	uint8_t Parameter[10];
	uint8_t * Buffer;
	uint16_t Index;
} Filter;

