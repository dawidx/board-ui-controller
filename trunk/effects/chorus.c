/**
* @file This file contains methods related to the chorus effect
* @desc Chorus effect is like an echo effect except occurring very frequently. Just like
* voices in the choir can't sing all at exactly the same time. Some come earily some latter 
**/

#include "chorus.h"

/**
* @desc The function applies transformation in accordance to the given a particular Chorus effect properties.
* @param uint8_t value is the amplitude
* @param uint16_t samplingRate is the sampling rate. It is used to determine the buffer offset
* @param Filter *filter is the pointer to the chorus effect
* @return uint8_t transformed value 
**/
uint8_t Get_Chorus_Value(uint8_t value, uint16_t samplingRate, Filter *filter) {
	// _DBG("Generating the chorus value");
	
	//Saving the value to the buffer BEFORE applying any changes on the value to avoid echo effect
	filter->Buffer[filter->Index] = value;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Input Value: %d \n\r ", value);
		_DBG(debugMessageBuffer);
	}
	// _DBG("Stored the value in the buffer");
	//Calculating the offset
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Chorus Time value: %d \n\r ", Get_Chorus_Time_Value(filter));
		_DBG(debugMessageBuffer);
	}
	int off = (Get_Chorus_Time_Value(filter) * samplingRate / 1000);
	//We don't want negative numbers, at no more that 1 
	float vary = (sin(Get_Chorus_Angle(filter)) + 1.0) * 0.5;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Chorus Time value: %f \n\r ", vary);
		_DBG(debugMessageBuffer);
	}
	uint16_t offset = Get_Chorus_Index_Offset(off, off, filter);
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Offset: %d \n\r ", offset);
		_DBG(debugMessageBuffer);
	}
	// _DBG("Calculated offset");
	// Calculating the total volume to use relative values for each guitar
	uint8_t total_volume = Get_Chorus_Voice1_Volume(filter) + Get_Chorus_Voice2_Volume(filter);
	//Calculating the amplitude with the volumes of both voices taken in to account
	uint8_t amplitude = (Get_Chorus_Voice1_Volume(filter) * filter->Buffer[filter->Index]) / total_volume + (Get_Chorus_Voice2_Volume(filter) * filter->Buffer[offset]) / total_volume;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " New Value: %d \n\r ", value);
		_DBG(debugMessageBuffer);
	}
	// _DBG("Calculated new amplitude");
	//Incrementing the index for the next
	filter->Index = Get_Chorus_Index_Offset(1, off, filter);
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " New Index: %d \n\r ", filter->Index);
		_DBG(debugMessageBuffer);
	}
	// _DBG("Updated the index");
	//returning the output amplitude value
	return amplitude;
}

/**
* @desc The function generates new chorus effect
* @return Filter chorus
**/
Filter Generate_Chorus_Effect() {
	
	Filter filter;
	filter.ID = CHORUS_EFFECT_ID;
	filter.Index = 0;
	//Volume, ChorusTime, Volume of Voice 1, Volume of Voice 2;
	Set_Effect_Volume(100, &filter);
	Set_Chorus_Time_Value(80, &filter);
	Set_Chorus_Voice1_Volume(100, &filter);
	Set_Chorus_Voice2_Volume(100, &filter);
	Set_Chorus_Time_Variant(2, &filter);
	//Due to limited memory we can allocate the maximim "delay" of 80ms at 44.1 kHz
	uint8_t effectArray[3528];
	filter.Buffer = effectArray;
	// _DBG("Allocated memory\n\r");

	return filter;
}

/**
* @desc The function generates the offset value based on the sampling rate and current chorus time value
* @return uint16_t Offset value
**/
uint16_t Generate_Chorus_Offset(Filter *filter, uint16_t samplingRate) {
	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Offset()\n\r");
	return Get_Chorus_Time_Value(filter) * samplingRate / 10000;
}

/**
* @desc Function used for calculating Index in the chorus buffer
* @return Index
**/
uint16_t Get_Chorus_Index_Offset(uint16_t offset, uint16_t max, Filter *filter) {
	if (offset <= max) {
		uint16_t location = filter->Index + offset + 1;
		if (location >= max) {
			location = location - max;
		}
		return location;
	}
	return 0;
}

/**
* @desc The function sets the value of the chorus effect i.e. the microseconds the signal is delayed
* @return bool true if assignment successful, false otherwise
**/
bool Set_Chorus_Time_Value(uint8_t value, Filter *filter) {

	// _DBG("Entered the effects/chorus.c/Set_Chorus_Time_Value()\n\r");
	if (value <= 80 && value >= 50) {
		filter->Parameter[1] = value;
		return true;
	}
	else
	{
		return false;
	}
}
/**
* @desc Functions used for extracting Chorus Time i.e. by how many microseconds the signal is delayed
* @return uint8_t the value of the chorus time
**/
uint8_t Get_Chorus_Time_Value(Filter *filter) {
	return filter->Parameter[1];
}



/**
* @desc Sets the value for volume for the original signal
* @return bool true if assignment successful, false otherwise (i.e. maximum volume)
**/
bool Set_Chorus_Voice1_Volume(uint8_t value, Filter *filter) {

	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Voice1_Volume()\n\r");
	if (value <= 100 && value >= 0) {
		filter->Parameter[2] = value;
		return true;
	}
	else {
		return false;
	}
}

/**
* @desc gets the value for volume of the original signal
* @return uint8_t volume of first gitar
**/
uint8_t Get_Chorus_Voice1_Volume(Filter *filter) {
	return filter->Parameter[2];
}

/**
* @desc Sets the value for volume for the delayed signal
* @return bool true if assignment successful, false otherwise (i.e. maximum volume)
**/
bool Set_Chorus_Voice2_Volume(uint8_t value, Filter *filter) {
	// _DBG("Entered the effects/chorus.c/Generate_Chorus_Voice2_Volume()\n\r");
	if (value <= 100 && value >= 0) {
		filter->Parameter[3] = value;
		return true;
	}
	else {
		return false;
	}
}

/**
* @desc gets the value for volume of the delayed signal
* @return 
**/
uint8_t Get_Chorus_Voice2_Volume(Filter *filter) {
	return filter->Parameter[3];
}
/**
* @desc gets the current chorus time variant
**/
bool Set_Chorus_Time_Variant(uint8_t value, Filter *filter) {
	//between 1-10
	if (value >= 1 && value <= 10) {
		filter->Parameter[4] = value;
		return true;
	}
	else {
		return false;
	}
}
/**
* @desc Returns how many "small vibratos" is in the time delay;
* @param Filter *filter pointer to the filter
**/
uint8_t Get_Chorus_Time_Variant(Filter *filter) {
	return filter->Parameter[4];
}
/**
* @desc Returns the angle of the sine wave. Depending on the Time Variant and current index
**/
float Get_Chorus_Angle(Filter *filter) {
	return (2.0 * PI * ((float)filter->Index / (3528.0 / (float)Get_Chorus_Time_Variant(filter))));
}