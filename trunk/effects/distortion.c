/**
* @file Distortion effect compresses the pick of the wave such that they are almost square
**/
#include "distortion.h"

/**
* @desc The function returns the value of the sample when it passes through distortion effect
* @param uint8_t amplitude The sample value
**/
uint8_t Get_Distortion_Value(uint8_t amplitude, Filter *filter)
{
	/**
	if(DEBUG) {
		_DBG("Get_Distortion_Effect_Value\n\r");
		debugMessageLength = sprintf(debugMessageBuffer, "Input %d \n\r", amplitude);
		_DBG(debugMessageBuffer);
	}
	uint8_t value;
	
	//Threshld applied for picks
	uint8_t max = 255 - Get_Distortion_Threshold(filter);
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, "MAX: %d \n\r ", max);
		_DBG(debugMessageBuffer);
	}
	uint8_t min = Get_Distortion_Threshold(filter);
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, "MIN: %d \n\r ", min);
		_DBG(debugMessageBuffer);
	}
	if(amplitude <= min)
	{
		value = min;
		if(DEBUG) {
			_DBG("Thresholding Applied\n\r");
		}
	}
	else if (amplitude >= max) {
		value = max;
		if(DEBUG) {
			_DBG("Thresholding Applied\n\r");
		}
	}
	else// if(amplitude < g_DistortionThreshold)
	{
		value = amplitude;
	}
	*/
	//@see http://www.sgm-audio.com/research/dist/dist.html
	//Getting the angle
	float angle = asin(((float)amplitude-128.0)/128.0);
	float value = 0;
	//Adding up terms only odd harmonics Fourier series
	int i;
	uint8_t threshold = Get_Distortion_Threshold(filter);
	for (i = 1; i <= threshold; i += 2) {
		value += (2.0 * sin((float)i*angle)) / ((float)i*(float)PI);
	}
	//The wave from above will oscilate somewhere between -0.5 and +0.5
	//Making it bigger and adding offset
	value = (value * 150) + 128;
	uint8_t output = 0;
	if(value > 255.0) {
		output = 255;
	} else if(value < 0.0) {
		output = 0;
	} else {
		output = (uint8_t) value;
	}
	return (uint8_t)output;
}
/**
* @desc Generates a new distortion filter
* @return Filter
**/
Filter Generate_Distortion_Effect() {
	Filter filter;
	uint8_t array[0];
	filter.Buffer = array;
	filter.Index = 0;
	filter.ID = DISTORTION_EFFECT_ID;
	if(Set_Effect_Volume(30, &filter)) {
		_DBG("Successfully applied volume to distorion effect \n\r");
	}
	if(Set_Distortion_Threshold(10, &filter)) {
		_DBG("Successfully applied threshold to distorion effect \n\r");
	}
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, "Threshold set to: %d \n\r ", filter.Parameter[1]);
		_DBG(debugMessageBuffer);
	}
	return filter;
}
/**
* @desc Sets the threshold for the distorion effect
* @return true if the value successfully applied, false otherwise (i.e. limit has been reached)
**/
bool Set_Distortion_Threshold(int8_t value, Filter *filter) {
	if (value <= 30 && value >= 1) {
		if(DEBUG) {
			debugMessageLength = sprintf(debugMessageBuffer, "Threshold: %d \n\r ", value);
			_DBG(debugMessageBuffer);
		}
		filter->Parameter[1] = value;
		if(DEBUG) {
			debugMessageLength = sprintf(debugMessageBuffer, "Threshold set to: %d \n\r ", filter->Parameter[1]);
			_DBG(debugMessageBuffer);
		}
		return true;
	} else {
		return false;
	}
}
/**
* @desc Gets the threshold value
* @return uint8_t threshold value
**/
uint8_t Get_Distortion_Threshold(Filter *filter) {
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, "Threshold: %d \n\r ", filter->Parameter[1]);
		_DBG(debugMessageBuffer);
		}
	return filter->Parameter[1];
}