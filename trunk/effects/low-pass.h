uint8_t Get_LowPass_Value(uint8_t value, uint16_t samplingRate, Filter *filter);
uint16_t Get_LowPass_Index_Offset(uint16_t offset, Filter *filter);
Filter Generate_LowPass_Effect();
bool Set_LowPass_Breakpoint_Frequency(uint16_t frequency, Filter *filter);
uint16_t Get_LowPass_Breakpoint_Frequency(Filter *filter);