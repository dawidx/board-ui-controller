/**
* Main file responsible for calling the controller
**/
#include <stdbool.h>
#include "debug_frmwrk.h"		// Debug messages
#include "effects/filter.h"
#include "include/control.c"
/**c
* @desc The method that ccis called when the controler is started
**/

void main(void)
{
	debug_frmwrk_init(); // Initialise the debug system to allow messages to be printed to the emulated terminal
	_DBG(	"\n\r\n\r>>>\n\r"
		"__________________________________________\n\r"
		"----------ADC->DAC Effects Filter---------\n\r"
		"__________________________________________\n\r"
		"\t\t(DAC output is Pin 18)\n\r\n\r"		
		"\t\t(ADC input is Pin 15)\n\r\n\r");
	_DBG("Entered the main/main()\n\r");

	Boot_Up(); // Call to the control file
	


	// We don't want to accidently execute any junk code that may be stored, nor we want to stop the execution of the program hence the while loop.

	while(1);
}