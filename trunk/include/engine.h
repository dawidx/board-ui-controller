uint8_t Apply_Effect(Filter *filter, uint8_t value);
void applyChanges();
void Reset_Filters();

/**
* @desc The functions checks the available memory. It is called in SetEffect in order to make sure there is a sufficent memory to create an effect
* @global g_AvailableMemory
* @param uint8_t effectID the effect ID
*/
bool Check_Available_Memory(uint8_t effectID);

/**
* @global g_MasterVolume
* @desc Returns the current system volume.
**/
uint8_t Get_Volume();
/**
* @desc Increments the system volume
* @return bool True if the value has been successfully updated, otherwise returns false i.e. when the value is at maximum
**/
bool IncreaseVolume(uint8_t currentVolume);
/**
* @desc Decrements the system volume
* @return bool True if the value has been successfully updated, otherwise returns false => when the value is at minimum
**/
bool DecreaseVolume(uint8_t currentVolume);
/**
* @global g_MasterVolume
* @desc Sets the system volume to the parameter.
* @param uint8_t volume The target system volume.
* (not called externally)
**/
bool SetVolume(uint8_t volume);
/**
* @desc Returns the name of the effect specified by the effect ID.
* @param uint8_t effectID The ID of the effect.
**/


char* Get_EffectName(uint8_t effectID);
/**
* @desc Returns the ID of the effect selected for the channel and location passed.
* @param uint8_t channel The channel of the effect to look up.
* @param uint8_t location The location within the channel of the effect to look up.
**/
uint8_t Get_EffectID(uint8_t channel, uint8_t location);
/**
* @desc Returns an array of the effect IDs available for selection at the channel and location passed.
* @param uint8_t channel The channel of the effect to look up available effects for.
* @param uint8_t location The location within the channel of the effect to look up available effects for.
**/
uint8_t* Get_AllAvailableEffects(uint8_t channel, uint8_t location);
/**
* 
**/
uint8_t Get_NumEffects(uint8_t channel, uint8_t location);
/**
* @desc Resets the effect's variables to all its default values. Returns 1 if successful, or 0 if an invalid channel / location selected.
* @param uint8_t channel The channel of the effect to reset.
* @param uint8_t location The location within the channel of the effect to reset.
**/
bool ResetEffectDefaults(uint8_t channel, uint8_t location);
/**
* @desc Clears the effect and replaces it with a pass through effect. Returns 1 if successful, or 0 if an invalid channel / location selected.
* @param uint8_t channel The channel of the effect to clear.
* @param uint8_t location The location within the channel of the effect to clear.
**/
bool ClearEffect(uint8_t channel, uint8_t location);
/**
* @desc Creats a new filter
* @param uint8_t channel The channel of the effect to set.
* @param uint8_t location The location within the channel of the effect to set
**/
bool SetEffect(uint8_t channel, uint8_t location, uint8_t effectID);
/**
* @desc Returns the number of modifable variables for the effect passed.
* @param uint8_t effectID The ID of the effect to check.
**/
uint8_t Get_NumberEffectVariables(uint8_t effectID);
/**
* @desc Returns the name of the variable and effect selected.
* @param uint8_t effectID The effect to check the variable name for.
* @param uint8_t variableNumber the index of the variable to return the name for.
**/
char* Get_EffectVariableName(uint8_t effectID, uint8_t variableNumber);
/**
* @desc Returns the maximum value of the variable for the effect selected. The variable is always expressed as an integer.
* @param uint8_t effectID The effect to check.
* @param uint8_t variableNumber the index of the variable.
* @return Returns "0" if an invalid effectID / variableNumber is selected.
* @depreciated
**/
//uint8_t Get_EffectVariableMaxValue(uint8_t effectID, uint8_t variableNumber);
/**
* @desc Returns the minimum value of the variable for the effect selected. The variable is always expressed as an integer.
* @param uint8_t effectID The effect to check.
* @param uint8_t variableNumber the index of the variable.
* @return Returns "0" if an invalid effectID / variableNumber is selected.
* @depreciated
**/
//uint8_t Get_EffectVariableMinimumValue(uint8_t effectID, uint8_t variableNumber);
/**
* @desc Returns the current value for the variable of the effect at the channel and location selected.
* @param uint8_t channel The channel of the effect to look up variable value for.
* @param uint8_t location The location within the channel of the effect to look up variable value for.
* @param uint8_t variableNumber The index of the variable of the effect within the location of the channel for which to return the value for.
* @return Returns "0" if an invalid variableNumber / channel / location is selected.
**/
uint8_t Get_EffectVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber);
/**
* @desc Increments / decrements the variable within the effect stored at the location and chanel passed.
* @param uint8_t channel The channel of the effect to increment / decrement the variable for.
* @param uint8_t location The location within the channel of the effect to increment / decrement the variable for.
* @param uint8_t variableNumber The index of the variable of the effect within the location of the channel to increment / decrement the variable for.
* Returns 0 if an invalid channel / location / variableNumber is selected.
**/
bool IncrementVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber);
bool DecrementVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber);

bool SetVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber, uint8_t value);

bool Set_Effect_Volume(uint8_t volume, Filter *filter);

uint8_t Get_Effect_Volume(Filter *filter);

/**
* @desc Noise gate is used to remove quiet signal
**/
uint8_t Apply_Noise_Gate(uint8_t value);

/**
* @desc Compression is apply to boost up those parts of the signal that matter
**/
uint8_t Apply_Compression(uint8_t value);
uint8_t Apply_Spike_Gate(uint8_t value);