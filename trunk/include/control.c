/**
 * @file Contains all the functions required to run the various aspects of the program's functionality.
 */

#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "adc.h"
#include "dac.h"
#include "config.h"
#include "control.h"
#include "gpio.h"
#include "engine.c"
#include "serial.c"
#include "userInterface.c"
#include <stdio.h> // For sprintf; required for debugging (passing strings containing variables to the emulated console)


// Buffersize defined in config.h
/*
* From the new version the buffer is no longer here
*/
//uint8_t g_InputBuffer[BUFFERSIZE];
//uint16_t g_InputBufferStart = 0;	//The buffer starts at 0 and it is a 16bit (2^16 = 65536)
/* Use looping buffer, the buffer starts at 
*  g_InputBuffer[g_InputBufferStart], loops from 
*  g_InputBuffer[BUFFERSIZE] to g_InputBuffer[0] then
*  carries on to g_InputBuffer[g_InputBufferStart - 1].
*  The most recent sample is stored at 
*  g_InputBuffer[g_InputBufferStart], and the oldest at
*  g_InputBuffer[g_InputBufferStart-1].
*/

// uint32_t sampleRate = 200000;

// Set to 0 at after input, set to 1 when output complete, set to 2 when effects computation complete.
uint8_t effectsComplete = 2;

// For Debugging, global from config.h to save memory
char debugMessageBuffer[0xFF];
int debugMessageLength;



#define LED_LENGTH 4
const int ledPinnum[] = { 18, 20, 21, 23 };

int number = 0;

void gpio_init() {
	int i=0;
	PINSEL_CFG_Type 	PinCfg;

	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	
	PinCfg.Portnum = 1;

	for(; i<LED_LENGTH; i++) {
		PinCfg.Pinnum = ledPinnum[i];
		PINSEL_ConfigPin(&PinCfg);

		GPIO_SetDir(1, 1<<ledPinnum[i], 1);  
	}
}

void led_on(int id) {
	GPIO_SetValue(1, 1<<ledPinnum[id]);
}

void led_off(int id) {
	GPIO_ClearValue(1, 1<<ledPinnum[id]);
}

void delay() {
	volatile int i=0;
	for(;i<10000000;i++);
}

void display_number(int x) {
	int i=1;
	int p=0;
	for(; i<16; i<<=1) {		
		if(x&i) {
			led_on(p);
		} else {
			led_off(p);
		}

		p++;
	}
}


/**
* @desc Boot_Up Boots up the entire system. It is used for initialising: ADC, DAC and UI Components
**/
void Boot_Up() {
	_DBG("Entered include/control.c/Boot_Up()\n\r");	
	// Initialise user interface
	serial_init();
	gpio_init();
	_DBG("UI initialised\n\r");
	// Enable interrupts for ADC conversion completing.
	Initialize_ADC(MAX_SAMPLING_RATE); // ADC used to read input values to the input buffer. Call to adc.h
	_DBG("ADC Intialized\n\r");
	Initialize_DAC();
	_DBG("DAC Intialized\n\r");
	// Set ADC to continuously sample.
	ADC_StartCmd (LPC_ADC, ADC_START_CONTINUOUS);
	_DBG("ADC setup in ADC_START_CONTINUOUS mode\n\r");
	// Set ADC to start converting.
	ADC_BurstCmd (LPC_ADC, ENABLE);
	_DBG("ADC started sampling\n\r");
	// Prepare GPIO for checking whether to use UI or not
	//Initialize_GPIO();
	
	Reset_Filters();
	_DBG("Reset all filters to pass through\n\r");
	
	_DBG("ADC IRQ Enabling\n\r");

	NVIC_EnableIRQ(ADC_IRQn);
	// ALL STATEMENTS AFTER THIS POINT ARE NOT EXECUTED, THE INTERRUPT ROUTINE TAKES OVER
}

/**
* @desc The ADC interrupt handler, gets called every time an Analouge to Digital conversion completes. (Once each sample rate)
**/
void ADC_IRQHandler (void)
{
	// _DBG("Entered ADC interrupt service routine\n\r");
	// Disable the interrupt. If we don't want to get override the samples
	NVIC_DisableIRQ(ADC_IRQn);

	// if (effectsComplete != 2)
	// {
	// 	// Sample rate too high, cannot compute effects in time, reduce sample rate
	// 	g_sampleRate = reduceSampleRate(g_sampleRate);
	// 	Update_ADC_Sample_Rate(g_sampleRate);
	// 	effectsComplete = 1;
	// }

	// _DBG("Control.c\tADC_IRQHandler\t83\tGetting Data from ADC\n\r");
	// uint8_t value = ADC_Acquire_Data();
	uint8_t value = ADC_Acquire_Data();
	

	// Check the user interface for any updated parameters, if the GPIO enable UI
	// button is pressed.
	// _DBG("Control.c\tADC_IRQHandler\t87\tChecking UI\n\r");
	

	effectsComplete = 0;
	// Apply effects
	// _DBG("Control.c\tADC_IRQHandler\t95\tStart applying effects\n\r");
	uint16_t output = Apply_Effects(value);
	if(DEBUG) {
				debugMessageLength = sprintf(debugMessageBuffer, " Output Value: %d \n\r ", output);
				_DBG(debugMessageBuffer);
			}
	effectsComplete = 1;
	// _DBG("Control.c\tADC_IRQHandler\t99\tEnd applying effects, output\n\r");
	// Send the value after effects applied to the DAC
	// DAC_Output_Data(value);
	DAC_Output_Data(output);

	static int pack;
	static uint8_t buf[20] = {};
	// End data aquisition section
	//waiting for a packet to arrive
	display_number(0);
	pack += read_usb_serial_none_blocking(&buf[0], 1);
	if(pack != 0) {
		display_number(1);
		while(pack != 20) {
			pack += read_usb_serial_none_blocking(&buf[pack], 1);
		}
		display_number(3);
		delay();
		//reseting back to 0
		if(buf[0] == 0) {
			delay();
			//applying effects
			display_number(7);
			SetEffect(buf[1], buf[2], buf[3]);
			int i;
			for(i = 0; i < Get_NumberEffectVariables(buf[3]); i++) {
				SetVariableCurrentValue(buf[1], buf[2], i, buf[4+i]);
			}
			display_number(15);
		} else {

		display_number(5);
		delay();
			//modyfing volume
		}

		display_number(15);
	}
	
	pack = 0;


	NVIC_EnableIRQ(ADC_IRQn);

	effectsComplete = 2;
}

/**
* @desc Returns a reduced sample rate for the ADC.
* @param uint32_t samplingRate The current sampling rate of the system.
* @
**/ 
uint32_t reduceSampleRate(uint32_t samplingRate)
{
	//The minimum sampling rate is (default) 100Hz
	if (samplingRate >= MIN_SAMPLING_RATE)
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Reducing sample rate from %ld Hz ", samplingRate);
		_DBG(debugMessageBuffer);

		samplingRate -= 10; //Substracting 10 for each sample

		debugMessageLength = sprintf(debugMessageBuffer, "to %ld Hz\n\r", samplingRate);
		_DBG(debugMessageBuffer);
	}
	else
	{
		// SampleRateError
		_DBG("Minimum sample rate of 100 Hz is insufficient.\n\r");
	}

	return samplingRate;
}


/**
* @desc Error procedure containing the message and the source of the error
* @param message a string containing the type of error
* @param severity a uint8_t containing values 1 being high and 8 being low severity
**/
void Error_Report(char* message, uint8_t severity)
{
	
}