/**
 * @file Contains all the enumerations and function prototypes for interacting with the low level i2c calls,
 * required for the user interface to function correctly.
 */

/**
* @desc Enumeration for the status of the transfer over i2c: not currently transferring, transferring lcd data, transferring keypad data.
**/
typedef enum {NO_TRANSFER, LCD_TRANSFER, KEYPAD_TRANSFER} i2c_transferStatus_t;
void setI2CTransferStatus(i2c_transferStatus_t status);
i2c_transferStatus_t getI2CTransferStatus();
void blockAndSetI2CTransferStatus(i2c_transferStatus_t status);
void I2C1_IRQHandler(void);
void I2C0_IRQHandler(void);
void I2C2_IRQHandler(void);
void LCDTransferCallback(void);
void KeypadTransferCallback(void);
void i2c_init(void);
void LCD_init(void);
bool sendDataI2C(uint8_t *Transmit_Buffer, int length, i2c_transferStatus_t device, uint8_t *recieve, uint8_t recieveLength);
bool LCD_transmit_string(char* string, uint8_t length);
bool LCD_transmit_newline();
bool LCD_screen_clear(int method);
bool checkKeypad(uint8_t columnValues);
char keypadCharFromCode(char keyReceived);
char getInput();