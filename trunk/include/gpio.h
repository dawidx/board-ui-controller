/**
 * @file Contains all the required functionality for use of the DAC.
 */
#include "lpc17xx_gpio.h"

/**
 * @desc Starts up and enables the GPIO.
 */
void Initialize_GPIO()
{
	// @TODO: Set up GPIO pin and directions for correct port
	// PINSEL_CFG_Type PinCfg; // Pin configuration
	// PinCfg.Funcnum   = PINSEL_FUNC_2;
	// PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	// PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	// PinCfg.Portnum   = PINSEL_PORT_0;
	// PinCfg.Pinnum    = PINSEL_PIN_26;

   	// PINSEL_ConfigPin(&PinCfg);
	
   	GPIO_SetDir(2, 1<<5, 0);
}

/**
 * @desc Reads the value of the GPIO switch
 * @return True if the switch is pressed, false if it is not.
 */
bool GPIO_Read_Value()
{
	uint32_t input=GPIO_ReadValue(2);
	 if((input & 1<<5) == (1<<5))
	 {
	 	if(DEBUG)
	 	{
			_DBG("Keypad: True\n\r");
		}
		return true;

	 }
	 else
	 {
	 	if(DEBUG)
	 	{
			_DBG("Keypad: False\n\r");
		}
	 	return false;
	 }
}