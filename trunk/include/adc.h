#include "lpc17xx_adc.h"
#include "lpc17xx_dac.h"
#include "config.h"
#include <stdio.h> //sprintf for debugging, for sending strings containing variables to the emulated terminal

uint8_t Convert_12_To_8(uint16_t input);

/**
* @desc Initialize_ADC enables pins on the MBED Board
* @param int samplingRate. Sampling rate defines the number of samples per second.
* For most audio systems is 44.1 kHz
**/
void Initialize_ADC(uint32_t samplingRate)
{
	// Pin configuration for the MBED Board
	// The ADC Input is PIN 15 on the Board
	PINSEL_CFG_Type PinCfg; 
	PinCfg.Funcnum   = PINSEL_FUNC_1;
   	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_23;

   	PINSEL_ConfigPin(&PinCfg);
	// Set up the ADC sampling at the sample rate. This rate can be varied
	ADC_Init(LPC_ADC, samplingRate);

   	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_0, ENABLE);
}

/**
* @desc Update_ADC_Sample_Rate is called whenever the Sample Rate changes. For instance,
* this can be the result of not being able to compute all the selected effects. 
**/
void Update_ADC_Sample_Rate(uint32_t samplingRate)
{
	// Destroying the ADC sampling
	ADC_DeInit(LPC_ADC);
	// Initializing ADC sampling with the new sampling rate
	ADC_Init(LPC_ADC, samplingRate);
}

/**
* @desc ADC_Acquire_Data is used for getting the data from the relevant channel, converting it
* to an appropiate 8bit form and storing it in the buffer.
*/
uint8_t ADC_Acquire_Data()
{
	// ADC_ChannelGetData() is used to get value read on ADC channel 0
	uint16_t valueToStore16 = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_0);
	// Right shift to get most significant 8 bits from 12 bit ADC value
	return Convert_12_To_8(valueToStore16);

	/*
	* //Used for checking whether the value converted from 16 bits to 8 bits contains the values from the top
	* debugMessageLength = sprintf(debugMessageBuffer, "Sample:\t16bit:\t%d \t8bit:\t%d mV\n\r", valueToStore16, valueToStore8);
	* _DBG(debugMessageBuffer);
	*/

	//Storing the value in the appropate memory location
	/*
	* From new version this is not removed. Each filter has its own memory association
	*/
	//Update_Buffer(value);
	//Returning the value
}

/**
* @desc Convert_12_To_8 takes an 12 bit integer in a 16bit representation and converts it to an 8bit (keeping highest 8bits) by 
* shifting it right 4 places.
* @param uint16_t 16bit input value
**/
uint8_t Convert_12_To_8(uint16_t input)
{
	if (DEBUG)
	{
		char debugMessageBuffer[0xFF];
		int debugMessageLength;

		debugMessageLength = sprintf(debugMessageBuffer, "adc.h\tConvert_12_To_8\t77:\tADC 12bit Value:%d\t"
			"ADC 10bit Value: %d\tADC 8bit Value: %d\n\r", input, input >> 2, (uint8_t)(input >> 4));
		_DBG(debugMessageBuffer);
	}
	return (uint8_t)(input >> 4);
}

/**
* @desc The function takes the current index of the buffer and returns next location
* @param uint16_t currentIndex is the current 16 bit buffer index
* @depreciated
**/
// uint16_t Next_Buffer_Index(uint16_t currentIndex)
// {
// 	if (currentIndex == 0) {
// 		return BUFFERSIZE - 1;
// 	}
// 	else {
// 		return currentIndex - 1;
// 	}
// }

/**
* @global g_InputBufferStart
* @global g_InputBuffer
* @const BUFFERSIZE
* @desc The function add new value inside the buffer in the appropate memory location. The algorithm loops through the buffer. 
* Whenever the value is passed, the algorithm takes the current index and stores the value. The index is then DECREMENTED.
* Note: that the value is decremented. If we want to see the value from the past we ADD the number of relevant memory locations
* @see Get_Buffer_Offset
* @param uint8_t value. The value to be stored in the next buffer location. It must be exactly 8 bits.
* @depreciated
*/
// void Update_Buffer(uint8_t value)
// {
// 	// inputBuffer[((g_InputBufferStart - 1) Mod BUFFERSIZE)].
// 	uint16_t nextBufferStartLocation = Next_Buffer_Index(g_InputBufferStart);
// 	g_InputBuffer[nextBufferStartLocation] = value;
// 	g_InputBufferStart = nextBufferStartLocation;	
// }
