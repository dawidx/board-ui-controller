/**
* @file User interface implementation using the LCD display, keypad and debug terminal.
**/

/**
 * Definitions of buttons used for input.
 */
#define BACK '*'
#define ACCEPT '#'
#define LEFT '1'
#define RIGHT '3'
#define RESET 'A'
#define SHOWCONFIGUREDEFFECTS 'B'

// Required for UI calls
#include "userInterfaceIO.c"
// Contains function prototypes.
#include "userInterface.h"

// For Debugging / virtual console output, global from config.h to save memory
char debugMessageBuffer[0xFF];
int debugMessageLength;

// Variable to store the current state of the user interface.
static Menu_Configuration g_menuCf;

/**
 * @desc Sets up i2c devices (through calls to userInterfaceIO.c), the initial state
 * of the user interface, and shows the initial output on the LCD.
 */
void UI_init()
{
	// Initialise i2c for LCD and Keypad
	// if(DEBUG)
	{
		_DBG("Begin i2c init\n\r");
	}
	i2c_init();
	// if(DEBUG)
	{
		_DBG("i2c initialised, begin LCD init\n\r");
	}
	// Setup LCD using i2c
	LCD_init();
	// if(DEBUG)
	{
		_DBG("LCD initialised, begin LCD_screen_clear\n\r");
	}

	// Clear LCD using method 1
	LCD_screen_clear(1);
	
	// if(DEBUG)
	{
		_DBG("LCD screen cleared\n\r");
	}

	g_menuCf.state = st_TopLevel;
	g_menuCf.menuOption = 1;

	printUI(g_menuCf);

}

/**
 * @desc Requests current input from userInterfaceIO.c, then passes this to applyAction.
 */
void checkUI()
{
	if (DEBUG)
	{
		_DBG("CheckUI\n\r");
	}
	static int count = 0;
	int checkCountValue = g_SampleRate / 4;
	if (count == checkCountValue)
	{
		if (DEBUG)
		{
			_DBG("getInput()\n\r");
		}
		char input = getInput();

		// Apply the button press to the current state
		applyAction(input);
		count++;
	}
	else if (count > checkCountValue)
	{
		count = 0;
	}
	else
	{
		count++;
	}

	// Only check 2 times a second.
	
}

/**
 * @desc Changes the current state of the user interface in accordance with the user's last
 * button press, and also makes the relevent calls to engine.c to implement the changes
 * requested in the user interface.
 * @param input The most recent keypress by the user.
 */
void applyAction(char input)
{
	uint8_t numEffects;

	// char debugMessageBuffer[0xFF];
	// int debugMessageLength;

	if (input == ' ')
	{
		return;
	}
	else if (input == RESET)
	{
		Reset_Filters();
		if(DEBUG)
		{
			_DBG("Reset filters\n\r");
		}
	}
	else if (input == SHOWCONFIGUREDEFFECTS)
	{
		//Setting up every fiter to passthrough
		int a, b;
		_DBG("The current system configuration:\n\r");
		for (a = 0; a < MAX_PARALLEL; a++)
		{
			_DBG("\t[ ");
			for (b = 0; b < MAX_SERIES; b++)
			{
				_DBG(Get_EffectName(Get_EffectID( a, b)));
				_DBC('\t');
			}
			_DBG("]\n\r");
		}
	}
	else
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Before:\tState: %d\tMenuOption %d\tInput:%c\n\r", g_menuCf.state, g_menuCf.menuOption, input);
		_DBG(debugMessageBuffer);

		switch(g_menuCf.state)
		{
			case st_TopLevel:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 2;
						else
							g_menuCf.menuOption--;
						printUI(g_menuCf);
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 2)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						printUI(g_menuCf);
						break;
					case ACCEPT:
						if (g_menuCf.menuOption == 1) // Volume
						{
							g_menuCf.state = st_Volume;
							g_menuCf.menuOption = Get_Volume();
							printUI(g_menuCf);
						}
						else // Effects
						{
							g_menuCf.state = st_Effects;
							g_menuCf.menuOption = 1;
							printUI(g_menuCf);
						}
						break;
					default:
						break;
				}
				break;

			case st_Volume:
				switch (input)
				{
					case LEFT:
						DecreaseVolume(g_menuCf.menuOption);
						g_menuCf.menuOption = Get_Volume();
						break;
					case RIGHT:
						IncreaseVolume(g_menuCf.menuOption);
						g_menuCf.menuOption = Get_Volume();
						break;
					case ACCEPT:
						// Same as back
					case BACK:
						g_menuCf.state = st_TopLevel;
						g_menuCf.menuOption = 1;
						printUI(g_menuCf);
						break;
					default:
						break;
				}
				break;

			case st_Effects:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch1;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch2;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								g_menuCf.state = st_ch3;
								g_menuCf.menuOption = 1;
								break;
							case 4:
								g_menuCf.state = st_ch4;
								g_menuCf.menuOption = 1;
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_TopLevel;
						g_menuCf.menuOption = 2;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;

			case st_ch1:
				// Same as st_ch2
			case st_ch2:
				// Same as st_ch3
			case st_ch3:
				// Same as st_ch4
			case st_ch4:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						if (g_menuCf.state == st_ch1)
						{
							switch(g_menuCf.menuOption)
							{
								case 1:
									g_menuCf.state = st_ch1ef1;
									g_menuCf.menuOption = 1;
									break;
								case 2:
									g_menuCf.state = st_ch1ef2;
									g_menuCf.menuOption = 1;
									break;
								case 3:
									g_menuCf.state = st_ch1ef3;
									g_menuCf.menuOption = 1;
									break;
								case 4:
									g_menuCf.state = st_ch1ef4;
									g_menuCf.menuOption = 1;
									break;
								default:
									break;
							}
						}
						else if (g_menuCf.state == st_ch2)
						{
							switch(g_menuCf.menuOption)
							{
								case 1:
									g_menuCf.state = st_ch2ef1;
									g_menuCf.menuOption = 1;
									break;
								case 2:
									g_menuCf.state = st_ch2ef2;
									g_menuCf.menuOption = 1;
									break;
								case 3:
									g_menuCf.state = st_ch2ef3;
									g_menuCf.menuOption = 1;
									break;
								case 4:
									g_menuCf.state = st_ch2ef4;
									g_menuCf.menuOption = 1;
									break;
								default:
									break;
							}
						}
						else if (g_menuCf.state == st_ch3)
						{
							switch(g_menuCf.menuOption)
							{
								case 1:
									g_menuCf.state = st_ch3ef1;
									g_menuCf.menuOption = 1;
									break;
								case 2:
									g_menuCf.state = st_ch3ef2;
									g_menuCf.menuOption = 1;
									break;
								case 3:
									g_menuCf.state = st_ch3ef3;
									g_menuCf.menuOption = 1;
									break;
								case 4:
									g_menuCf.state = st_ch3ef4;
									g_menuCf.menuOption = 1;
									break;
								default:
									break;
							}
						}
						else if (g_menuCf.state == st_ch4)
						{
							switch(g_menuCf.menuOption)
							{
								case 1:
									g_menuCf.state = st_ch4ef1;
									g_menuCf.menuOption = 1;
									break;
								case 2:
									g_menuCf.state = st_ch4ef2;
									g_menuCf.menuOption = 1;
									break;
								case 3:
									g_menuCf.state = st_ch4ef3;
									g_menuCf.menuOption = 1;
									break;
								case 4:
									g_menuCf.state = st_ch4ef4;
									g_menuCf.menuOption = 1;
									break;
								default:
									break;
							}
						}
						break;
					case BACK:
						g_menuCf.state = st_Effects;
						if (g_menuCf.state == st_ch1)
						{
							g_menuCf.menuOption = 1;
						}
						else if (g_menuCf.state == st_ch2)
						{
							g_menuCf.menuOption = 2;
						}
						else if (g_menuCf.state == st_ch3)
						{
							g_menuCf.menuOption = 3;
						}
						else if (g_menuCf.state == st_ch4)
						{
							g_menuCf.menuOption = 4;
						}
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;

			// 1: Choose Effect
			// 2: Configure Effect
			// 3: Reset Effect Defaults
			// 4: Clear Effect
			case st_ch1ef1:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch1ef1choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch1ef1configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(0, 0);
								break;
							case 4:
								ClearEffect(0, 0);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch1;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef2:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch1ef2choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch1ef2configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(0, 1);
								break;
							case 4:
								ClearEffect(0, 1);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch1;
						g_menuCf.menuOption = 2;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef3:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch1ef3choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch1ef3configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(0, 2);
								break;
							case 4:
								ClearEffect(0, 2);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch1;
						g_menuCf.menuOption = 3;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef4:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch1ef4choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch1ef4configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(0, 3);
								break;
							case 4:
								ClearEffect(0, 3);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch1;
						g_menuCf.menuOption = 4;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef1:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch2ef1choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch2ef1configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(1, 0);
								break;
							case 4:
								ClearEffect(1, 0);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch2;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef2:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch2ef2choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch2ef2configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(1, 1);
								break;
							case 4:
								ClearEffect(1, 1);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch2;
						g_menuCf.menuOption = 2;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef3:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch2ef3choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch2ef3configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(1, 2);
								break;
							case 4:
								ClearEffect(1, 2);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch2;
						g_menuCf.menuOption = 3;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef4:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch2ef4choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch2ef4configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(1, 3);
								break;
							case 4:
								ClearEffect(1, 3);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch2;
						g_menuCf.menuOption = 4;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef1:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch3ef1choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch3ef1configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(2, 0);
								break;
							case 4:
								ClearEffect(2, 0);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch1;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef2:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch3ef2choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch3ef2configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(2, 1);
								break;
							case 4:
								ClearEffect(2, 1);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch3;
						g_menuCf.menuOption = 2;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef3:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch3ef3choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch3ef3configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(2, 2);
								break;
							case 4:
								ClearEffect(2, 2);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch3;
						g_menuCf.menuOption = 3;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef4:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch3ef4choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch3ef4configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(2, 3);
								break;
							case 4:
								ClearEffect(2, 3);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch3;
						g_menuCf.menuOption = 4;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef1:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch4ef1choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch4ef1configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(3, 0);
								break;
							case 4:
								ClearEffect(3, 2);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch4;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef2:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch4ef2choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch4ef2configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(3, 1);
								break;
							case 4:
								ClearEffect(3, 1);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch4;
						g_menuCf.menuOption = 2;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef3:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch4ef3choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch4ef3configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(3, 2);
								break;
							case 4:
								ClearEffect(3, 2);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch4;
						g_menuCf.menuOption = 3;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef4:
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 1)
							g_menuCf.menuOption = 4;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == 4)
							g_menuCf.menuOption = 1;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						switch(g_menuCf.menuOption)
						{
							case 1:
								g_menuCf.state = st_ch4ef4choose;
								g_menuCf.menuOption = 1;
								break;
							case 2:
								g_menuCf.state = st_ch4ef4configure;
								g_menuCf.menuOption = 1;
								break;
							case 3:
								ResetEffectDefaults(3, 3);
								break;
							case 4:
								ClearEffect(3, 3);
								break;
							default:
								break;
						}
						break;
					case BACK:
						g_menuCf.state = st_ch4;
						g_menuCf.menuOption = 4;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;

			// Choose effect, menuOption is selected effect index
			case st_ch1ef1choose:
				numEffects = (Get_NumEffects( 0, 0)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 0, 0, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch1ef1;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef2choose:
				numEffects = (Get_NumEffects( 0, 1))- 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 0, 1, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch1ef2;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef3choose:
				numEffects = (Get_NumEffects( 0, 2)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 0, 2, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch1ef3;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef4choose:
				numEffects = (Get_NumEffects( 0, 3)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 0, 3, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch1ef4;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef1choose:
				numEffects = (Get_NumEffects( 1, 0)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 1, 0, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch2ef1;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef2choose:
				numEffects = (Get_NumEffects( 1, 1)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 1, 1, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch2ef2;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef3choose:
				numEffects = (Get_NumEffects( 1, 2)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 1, 2, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch2ef3;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef4choose:
				numEffects = (Get_NumEffects( 1, 3)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 1, 3, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch2ef4;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef1choose:
				numEffects = (Get_NumEffects( 2, 0)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 2, 0, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch3ef1;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef2choose:
				numEffects = (Get_NumEffects( 2, 1)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 2, 1, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch3ef2;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef3choose:
				numEffects = (Get_NumEffects( 2, 2)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 2, 2, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch3ef3;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef4choose:
				numEffects = (Get_NumEffects( 2, 3)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 2, 3, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch3ef4;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef1choose:
				numEffects = (Get_NumEffects( 3, 0)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 3, 0, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch4ef1;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef2choose:
				numEffects = (Get_NumEffects( 3, 1)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 3, 1, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch4ef2;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef3choose:
				numEffects = (Get_NumEffects( 3, 2)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 3, 2, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch4ef3;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef4choose:
				numEffects = (Get_NumEffects( 3, 3)) - 1;
				switch (input)
				{
					case LEFT:
						if (g_menuCf.menuOption == 0)
							g_menuCf.menuOption = numEffects;
						else
							g_menuCf.menuOption--;
						break;
					case RIGHT:
						if (g_menuCf.menuOption == numEffects)
							g_menuCf.menuOption = 0;
						else
							g_menuCf.menuOption++;
						break;
					case ACCEPT:
						SetEffect( 3, 3, g_menuCf.menuOption);
						break;
					case BACK:
						g_menuCf.state = st_ch4ef4;
						g_menuCf.menuOption = 1;
						break;
					default:
						break;
				}
				printUI(g_menuCf);
				break;		


			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified	
			case st_ch1ef1configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 0, 0) - 1);
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 0, 0, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 0, 0, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch1ef1;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef2configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 0, 1))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 0, 1, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 0, 1, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch1ef2;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef3configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 0, 2))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 0, 2, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 0, 2, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch1ef3;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch1ef4configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 0, 3))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 0, 3, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 0, 3, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch1ef4;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef1configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 1, 0))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 1, 0, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 1, 0, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch2ef1;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef2configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 1, 1))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 1, 1, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 1, 1, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch2ef2;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef3configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 1, 2))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 1, 2, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 1, 2, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch2ef3;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch2ef4configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 1, 3))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 1, 3, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 1, 3, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch2ef4;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef1configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 2, 0))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 2, 0, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 2, 0, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch3ef1;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef2configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 2, 1))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 2, 1, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 2, 1, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch3ef2;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef3configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 2, 2))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 2, 2, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 2, 2, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch3ef3;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch3ef4configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 2, 3))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 2, 3, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 2, 3, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch3ef4;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef1configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 3, 0))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 3, 0, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 3, 0, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch4ef1;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef2configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 3, 1))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 3, 1, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 3, 1, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch4ef2;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef3configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 3, 2))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 3, 2, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 3, 2, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch4ef3;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;
			case st_ch4ef4configure:
				numEffects = Get_NumberEffectVariables(Get_EffectID( 3, 3))-1;
				if ((g_menuCf.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
				{
					switch (input)
					{
						case LEFT:
							// Remove MSB for veriable index for decrease
							DecrementVariableCurrentValue( 3, 3, g_menuCf.menuOption & 0x7F);
							break;
						case RIGHT:
							IncrementVariableCurrentValue( 3, 3, g_menuCf.menuOption & 0x7F);
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						case BACK:
							g_menuCf.menuOption = (g_menuCf.menuOption & 0x7F); // Set MSB to 0
							break;
						default:
							break;
					}
				}
				else // MSB == 0, choose parameter to edit
				{
					switch (input)
					{
						case LEFT:
							if (g_menuCf.menuOption == 0)
								g_menuCf.menuOption = numEffects;
							else
								g_menuCf.menuOption--;
							break;
						case RIGHT:
							if (g_menuCf.menuOption == numEffects)
								g_menuCf.menuOption = 0;
							else
								g_menuCf.menuOption++;
							break;
						case ACCEPT:
							g_menuCf.menuOption = (g_menuCf.menuOption | 0x80); // Set MSB to 1
							break;
						case BACK:
							g_menuCf.state = st_ch4ef4;
							g_menuCf.menuOption = 2;
							break;
						default:
							break;
					}
				}
				printUI(g_menuCf);
				break;

			default:
				// Other state, output the char to the emulated terminal.
				// LCD_transmit_char(input);
				_DBC(input);
				break;
		}
	}

	debugMessageLength = sprintf(debugMessageBuffer, "After:\tState: %d\tMenuOption %d\tInput:%c\n\r\n\r", g_menuCf.state, g_menuCf.menuOption, input);
	_DBG(debugMessageBuffer);
}

/**
 * @desc Draws the current user interface on the LCD device, with appropriate menu titles and
 * options for the current user interface state.
 * @param menuConfig The current state and selected options on the user interface.
 */
void printUI(Menu_Configuration menuConfig)
{
	switch (menuConfig.state)
	{
		case st_TopLevel:
			if (menuConfig.menuOption == 1) // Volume selected
			{
				LCD_transmit_string("Audio Options   ", 17);
				LCD_transmit_newline();
				LCD_transmit_string("<Volume        >", 17);
				LCD_transmit_newline();
			}
			else // Effects selected
			{
				LCD_transmit_string("Audio Options   ", 17);
				LCD_transmit_newline();
				LCD_transmit_string("<Effects       >", 17);
				LCD_transmit_newline();
			}
			break;
		case st_Volume:
			LCD_transmit_string("Edit Volume     ", 17);
			LCD_transmit_newline();
			LCD_transmit_string("<Volume        >", 17);
			LCD_transmit_newline();
			break;
		case st_Effects:
			LCD_transmit_string("Manage Effects  ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Channel 1     >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("<Channel 2     >", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("<Channel 3     >", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Channel 4     >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_string("----------------", 17);
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch1:
			LCD_transmit_string("Ch1 Effects     ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Effect 1      >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("<Effect 2      >", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("<Effect 3      >", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Effect 4      >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch2:
			LCD_transmit_string("Ch2 Effects     ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Effect 1      >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("<Effect 2      >", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("<Effect 3      >", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Effect 4      >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch3:
			LCD_transmit_string("Ch3 Effects     ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Effect 1      >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("<Effect 2      >", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("<Effect 3      >", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Effect 4      >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch4:
			LCD_transmit_string("Ch4 Effects     ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Effect 1      >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("<Effect 2      >", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("<Effect 3      >", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Effect 4      >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch1ef1:
			LCD_transmit_string("Ch1 Ef1 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch1ef2:
			LCD_transmit_string("Ch1 Ef2 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch1ef3:
			LCD_transmit_string("Ch1 Ef3 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch1ef4:
			LCD_transmit_string("Ch1 Ef4 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch2ef1:
			LCD_transmit_string("Ch2 Ef1 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch2ef2:
			LCD_transmit_string("Ch2 Ef2 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch2ef3:
			LCD_transmit_string("Ch2 Ef3 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch2ef4:
			LCD_transmit_string("Ch2 Ef4 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch3ef1:
			LCD_transmit_string("Ch3 Ef1 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch3ef2:
			LCD_transmit_string("Ch3 Ef2 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch3ef3:
			LCD_transmit_string("Ch3 Ef3 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch3ef4:
			LCD_transmit_string("Ch3 Ef4 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch4ef1:
			LCD_transmit_string("Ch4 Ef1 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch4ef2:
			LCD_transmit_string("Ch4 Ef2 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch4ef3:
			LCD_transmit_string("Ch4 Ef3 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch4ef4:
			LCD_transmit_string("Ch4 Ef4 Options ", 17);
			LCD_transmit_newline();
			switch (menuConfig.menuOption)
			{
				case 1:
					LCD_transmit_string("<Choose Effect >", 17);
					LCD_transmit_newline();
					break;
				case 2:
					LCD_transmit_string("Configure Effect", 17);
					LCD_transmit_newline();
					break;
				case 3:
					LCD_transmit_string("Reset Ef Default", 17);
					LCD_transmit_newline();
					break;
				case 4:
					LCD_transmit_string("<Clear Effect  >", 17);
					LCD_transmit_newline();
					break;
				default:
					LCD_transmit_newline();
					break;
			}
			break;
		case st_ch1ef1choose:
			LCD_transmit_string("Choose Ch1 Ef1  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch1ef2choose:
			LCD_transmit_string("Choose Ch1 Ef2  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();			
			break;
		case st_ch1ef3choose:
			LCD_transmit_string("Choose Ch1 Ef3  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch1ef4choose:
			LCD_transmit_string("Choose Ch1 Ef4  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch2ef1choose:
			LCD_transmit_string("Choose Ch2 Ef1  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch2ef2choose:
			LCD_transmit_string("Choose Ch2 Ef2  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch2ef3choose:
			LCD_transmit_string("Choose Ch2 Ef3  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch2ef4choose:
			LCD_transmit_string("Choose Ch2 Ef4  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch3ef1choose:
			LCD_transmit_string("Choose Ch3 Ef1  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch3ef2choose:
			LCD_transmit_string("Choose Ch3 Ef2  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch3ef3choose:
			LCD_transmit_string("Choose Ch3 Ef3  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch3ef4choose:
			LCD_transmit_string("Choose Ch3 Ef4  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch4ef1choose:
			LCD_transmit_string("Choose Ch4 Ef1  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch4ef2choose:
			LCD_transmit_string("Choose Ch4 Ef2  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch4ef3choose:
			LCD_transmit_string("Choose Ch4 Ef3  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch4ef4choose:
			LCD_transmit_string("Choose Ch4 Ef4  ", 17);
			LCD_transmit_newline();
			LCD_transmit_string(Get_EffectName(menuConfig.menuOption), 17);
			LCD_transmit_newline();
			break;
		case st_ch1ef1configure:
			LCD_transmit_string("Edit Ch1 Ef1    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 0, 0, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{				
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 0, 0), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch1ef2configure:
			LCD_transmit_string("Edit Ch1 Ef2    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 0, 1, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 0, 1), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch1ef3configure:
			LCD_transmit_string("Edit Ch1 Ef3    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 0, 2, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 0, 2), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch1ef4configure:
			LCD_transmit_string("Edit Ch1 Ef4    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 0, 3, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 0, 3), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch2ef1configure:
			LCD_transmit_string("Edit Ch2 Ef1    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 1, 0, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 1, 0), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch2ef2configure:
			LCD_transmit_string("Edit Ch2 Ef2    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 1, 1, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 1, 1), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch2ef3configure:
			LCD_transmit_string("Edit Ch2 Ef3    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 1, 2, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 1, 2), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch2ef4configure:
			LCD_transmit_string("Edit Ch2 Ef4    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 1, 3, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 1, 3), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch3ef1configure:
			LCD_transmit_string("Edit Ch3 Ef1    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 2, 0, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 2, 0), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch3ef2configure:
			LCD_transmit_string("Edit Ch3 Ef2    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 2, 1, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 2, 1), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch3ef3configure:
			LCD_transmit_string("Edit Ch3 Ef3    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 2, 2, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 2, 2), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch3ef4configure:
			LCD_transmit_string("Edit Ch3 Ef4    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 2, 3, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 2, 3), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch4ef1configure:
			LCD_transmit_string("Edit Ch4 Ef1    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 3, 0, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 3, 0), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch4ef2configure:
			LCD_transmit_string("Edit Ch4 Ef2    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 3, 1, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 3, 1), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch4ef3configure:
			LCD_transmit_string("Edit Ch4 Ef3    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 3, 2, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 3, 2), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
		case st_ch4ef4configure:
			LCD_transmit_string("Edit Ch4 Ef4    ", 17);
			LCD_transmit_newline();
			// Configure effect, menuOption is selected effect variable index,
			// menuOption's most significant bit is set to 1 if the variable is
			// being edited, and 0 if it is the variable to edit is being modified
			if ((menuConfig.menuOption & 0x80) == 0x80) // MSB == 1, edit parameter
			{
				char buffer [17] = "                ";
  				int count;
  				// Print current variable value
  				count = snprintf(buffer, 17, "%d", Get_EffectVariableCurrentValue( 3, 3, (menuConfig.menuOption & 0x7F)));
  				uint8_t i;
  				for (i = 0; i < 16; i++)
  				{
  					if(buffer[i] == '\0')
  					{
  						buffer[i] = ' ';
  					}
  				}
  				LCD_transmit_string(buffer, 17);
  				LCD_transmit_newline();
			}
			else // MSB == 0, choose parameter to edit.
			{
				LCD_transmit_string(Get_EffectVariableName(Get_EffectID( 3, 3), menuConfig.menuOption), 17);
				LCD_transmit_newline();
			}
			break;
			
		default:
			break;	
	}
}