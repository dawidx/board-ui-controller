/**
 * @file Contains all the required functionality for use of the DAC.
 */
#include "lpc17xx_dac.h"

/**
 * @desc Starts up and enables the DAC.
 */
void Initialize_DAC() {
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.Funcnum   = PINSEL_FUNC_2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_26;

   	PINSEL_ConfigPin(&PinCfg);
	
   	DAC_Init(LPC_DAC);
}


/**
 * @desc Outputs a value passed to it on the DAC device. Also converts the 8 bit representation to a 10 bit representation as required.
 * @param outputValue The value to set the DAC voltage as. Max 10 used bits
 */
void DAC_Output_Data(uint16_t outputValue)
{

	// Update DAC value (out of 0x400)
	// outputValue left shifted 2 bits to allow for different bit depth (8->10 bits)
	if(DEBUG)
	{
		debugMessageLength = sprintf(debugMessageBuffer, " DAC OUTPUT: %d \n\r ", outputValue);
		_DBG(debugMessageBuffer);
	}
	DAC_UpdateValue(LPC_DAC, outputValue<<2);

	if (DEBUG)
	{
		char debugMessageBuffer[0xFF];
		int debugMessageLength;

		debugMessageLength = sprintf(debugMessageBuffer,
			"dac.h\tDAC_Output_Data\t36:\tOutput Value(2l):%d Output Value: %d Output Value (2r): %d\n\r",
			(outputValue<<2), (outputValue), (outputValue>>2));
		_DBG(debugMessageBuffer);
	}
	return;
}