#include "../../effects/chorus.c"

void Chorus_Testing() {
	Chorus_Offset_Test_1();
	Chorus_Offset_Test_2();

	Chorus_Gen_Offset_Test_1();
	Chorus_Gen_Offset_Test_2();
}

void Chorus_Offset_Test_1() {
	//Test one just tests whether the correct value gets added
	Filter filter;
	filter.Index = 10;
	Equals(Get_Chorus_Index_Offset(filter, 10), 20);
	free(filter);
}

void Chorus_Offset_Test_2() {
	//Test two checks whether the algorithm correctly loops back
	Filter filter;
	filter.Index = 2100;
	Equals(Get_Chorus_Index_Offset(filter, 200), 100);
	free(filter);
}

void Chorus_Gen_Offset_Test_1() {
	//Tests whether the correct offset is generated
	//sampling rate
	uint16_t sr = 44000;
	Filter filter;
	Set_Chorus_Time_Value(50, filter);
	Equals(Generate_Chorus_Offset(filter, sr), 2200);
	free(filter);
	free(sr);
}

void Chorus_Gen_Offset_Test_2() {
	//Tests whether the correct offset is generated
	//sampling rate
	uint16_t sr = 20000;
	Filter filter;
	Set_Chorus_Time_Value(50, filter);
	Equals(Generate_Chorus_Offset(filter, sr), 1000);
	free(filter);
	free(sr);
}